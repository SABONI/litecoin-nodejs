from ontology.ont_sdk import OntologySdk
from ontology.wallet.wallet_manager import WalletManager
from ontology.wallet.account import AccountData

rpc_address = 'http://polaris1.ont.io:20336'
sdk = OntologySdk()
sdk.rpc.set_address(rpc_address)
base58_address = "ARGDdzWNQfiHarBqDW7h83sGaXwVkwEPRi"
address_balance = sdk.rpc.get_balance(base58_address)
ont_balance = address_balance['ont']
ong_balance = address_balance['ong']
print(ong_balance)

wm = WalletManager()
label = 'test'
password = 'aa12301230'
acct = wm.create_account(label, password)
wm.get_wallet()
accounts = wm.get_wallet().get_accounts()
print(accounts)
fun = accounts[0]
for s in dir(fun):
    print(s, ':', fun.__getattribute__(s))


from ontology.ont_sdk import OntologySdk
from ontology.account.account import Account
from ontology.crypto.signature_scheme import SignatureScheme
from ontology.smart_contract.native_contract.asset import Asset

rpc_address = 'http://polaris3.ont.io:20336'  # testnet
sdk = OntologySdk()
sdk.rpc.set_address(rpc_address)

private_key1 = "523c5fcf74823831756f0bcb3634234f10b3beb1c05595058534577752ad2d9f"
private_key2 = "75de8489fcb2dcaf2ef3cd607feffde18789de7da129b5e97c81e001793cb7cf"

from_acct = Account(private_key1, SignatureScheme.SHA256withECDSA)
to_acct = Account(private_key2, SignatureScheme.SHA256withECDSA)

amount = 1
gas_price = 500
gas_limit = 20000

b58_to_address = to_acct.get_address_base58()
b58_from_address = from_acct.get_address_base58()
b58_payer_address = from_acct.get_address_base58()
print('from:', b58_from_address, 'to:', b58_to_address, 'amount:', amount, 'gas_price:', gas_price, 'gas_limit:', gas_limit)
tx = Asset.new_transfer_transaction('ont', b58_from_address, b58_to_address, amount, b58_payer_address, gas_limit, gas_price)
print('signing')
tx = sdk.sign_transaction(tx, from_acct)
tx = sdk.add_sign_transaction(tx, to_acct)
print('sending')
tx_hash = sdk.rpc.send_raw_transaction(tx)
print('sent ', tx_hash)
