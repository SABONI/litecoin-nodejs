'''loop
n=19
m=10
for x in range(0,n):
    a=1
    for y in range(1,m):
        if(y%2 == 0):
            b = 10 - a
            a = a + 1
            output= x*b
            print("%s * %s = %s"%(x,b,output))
        else:
            output= x*a
            print("%s * %s = %s"%(x,a,output))   
'''
'''
A0 = dict(zip(('a','b','c','d','e'),(1,2,3,4,5)))
A1 = range(10)
A2 = [i for i in A1 if i in A0]
A3 = [A0[s] for s in A0]
A4 = [i for i in A1 if i in A3]
A5 = {i:i*i for i in A1}
A6 = [[i,i*i] for i in A1]
print(A0)
print(A1)
print(A2)
print(A3)
print(A4)
print(A5)
print(A6)
'''
'''
def f(x,l=[]):
    print(l)
    for i in range(x):
        l.append(i*i)
    print(l)
f(2)
f(3,[3,2,1])
f(4)
'''

'''繼承多型
class MyClass(object):
    def __init__(self):
        self._some_property = "properties are nice"
        self._some_other_property = "VERY nice"
    def normal_method(*args,**kwargs):
        print ("calling normal_method({0},{1})".format(args,kwargs))
    @classmethod
    def class_method(*args,**kwargs):
        print ("calling class_method({0},{1})".format(args,kwargs))
    @staticmethod
    def static_method(*args,**kwargs):
        print ("calling static_method({0},{1})".format(args,kwargs))
    @property
    def some_property(self,*args,**kwargs):
        print ("calling some_property getter({0},{1},{2})".format(self,args,kwargs))
        return self._some_property
    @some_property.setter
    def some_property(self,*args,**kwargs):
        print ("calling some_property setter({0},{1},{2})".format(self,args,kwargs))
        self._some_property = args[0]
    @property
    def some_other_property(self,*args,**kwargs):
        print ("calling some_other_property getter({0},{1},{2})".format(self,args,kwargs))
        return self._some_other_property

o = MyClass()
o.normal_method 
o.normal_method() 
o.normal_method(1,2,x=3,y=4) 

o.class_method
o.class_method()
o.class_method(1,2,x=3,y=4)


o.static_method
o.static_method()
o.static_method(1,2,x=3,y=4)


o.some_property
print(1)
#o.some_property()
print(2)
#o.some_other_property
print(3)
o.some_property = "groovy"
print(4)
o.some_property
print(5)
#o.some_other_property = "very groovy"
print(6)
#o.some_other_property
'''
'''decorate
def use_logging(func):
    def wrapper(*args,**kwargs):
        print("%s is running"%func.__name__)
        return func(*args)
    return wrapper

@use_logging
def foo():
    print("foo")

@use_logging
def bar():
    print("bar")
bar()
'''

'''繼承MRO
class A(object):
    def go(self):
        print ("go A go!")
    def stop(self):
        print ("stop A stop!")
    def pause(self):
        raise Exception("Not Implemented")

class B(A):
    def go(self):
        super(B, self).go()
        print ("go B go!")

class C(A):
    def go(self):
        super(C, self).go()
        print ("go C go!")
    def stop(self):
        super(C, self).stop()
        print ("stop C stop!")

class D(B,C):
    def go(self):
        super(D, self).go()
        print ("go D go!")
    def stop(self):
        super(D, self).stop()
        print ("stop D stop!")
    def pause(self):
        print ("wait D wait!")

class E(B,C,A): pass

a = A()
b = B()
c = C()
d = D()
e = E()

# 说明下列代码的输出结果

d.go()
print(D.__mro__)
print(E.__mro__)
'''
'''
class Node(object):
    def __init__(self,sName):
        self._lChildren = []
        self.sName = sName
    def __repr__(self):
        return "<Node '{}'>".format(self.sName)
    def append(self,*args,**kwargs):
        self._lChildren.append(*args,**kwargs)
    def print_all_1(self):
        print (self)
        for oChild in self._lChildren:
            oChild.print_all_1()
    def print_all_2(self):
        def gen(o):
            lAll = [o,]
            while lAll:
                oNext = lAll.pop(0)
                lAll.extend(oNext._lChildren)
                print("lALL:"+ lAll.__str__())
                print("oNext :" + oNext.__str__())
                yield oNext
        for oNode in gen(self):
            print (oNode)

oRoot = Node("root")
oChild1 = Node("child1")
oChild2 = Node("child2")
oChild3 = Node("child3")
oChild4 = Node("child4")
oChild5 = Node("child5")
oChild6 = Node("child6")
oChild7 = Node("child7")
oChild8 = Node("child8")
oChild9 = Node("child9")
oChild10 = Node("child10")

oRoot.append(oChild1)
oRoot.append(oChild2)
oRoot.append(oChild3)
oChild1.append(oChild4)
oChild1.append(oChild5)
oChild2.append(oChild6)
oChild4.append(oChild7)
oChild3.append(oChild8)
oChild3.append(oChild9)
oChild6.append(oChild10)

# 说明下面代码的输出结果

oRoot.print_all_1()
oRoot.print_all_2()
'''

'''
lis = [ 'a' , 'b' , 'c' , 'd' , 'e' ]
print  (lis [ :2])
def createStore(state, stateChanger):
    getState = lambda:state
    dispatch = lambda action:stateChanger(state,action)
    return {
            "getState":getState,
            "dispatch":dispatch
            }

appState = {
    "status":"good"
}

def stateChanger(state,action):
    if action == "good":
        state["status"] = "good"
    elif action == "bad":
        state["status"]="bad"

action = "bad"
store = createStore(appState,stateChanger)
print(store["getState"]())
store["dispatch"](action)
print(store["getState"]())
'''
'''lambda
z=lambda x:lambda y:x+y
print(z(2)(3))
'''

'''fabina
a,b=0,1
for x in range(0,100):
    a,b=b,a+b
    print(str(b)+','+str(x))
'''

'''set
ll=['a','b','d','d','s','a']
l1=set(ll)
print(l1)
'''

'''email
from email.mime.text import MIMEText
from email.header import Header
 
sender = 'from@runoob.com'
receivers = ['429240967@qq.com']  # 接收邮件，可设置为你的QQ邮箱或者其他邮箱
 
# 三个参数：第一个为文本内容，第二个 plain 设置文本格式，第三个 utf-8 设置编码
message = MIMEText('Python ...', 'plain', 'utf-8')
message['From'] = Header("from", 'utf-8')   # 发送者
message['To'] =  Header("to", 'utf-8')        # 接收者
 
subject = 'Python SMTP test'
message['Subject'] = Header(subject, 'utf-8')

print(message.as_string())
'''

'''and or
a ='betabin'
b ='python'
print(0 and a or b)
def choose(bool, a, b):return print(bool and a or b)
choose(True,a,b)
'''

'''引用計數
a={'a':'aa','b':'bb'}
b=a
print(id(a))
print(id(b))
a['a']='aaaaa'
print(id(a))
print(id(b))
print(b)
a = 1
b = 1
print(a is b)
a = "good"
b = "good"
print(a is b)
a = "very good morning"
b = "very good morning"
print(a is b)
a = []
b = []
print(a is b)
'''

'''ref
from sys import getrefcount
a = [1, 2, 3]
print(getrefcount(a))
b =[1,5,9]
a=b
print(getrefcount(b))
print(getrefcount(a))
'''

'''super __init___ __new___
class B(object):
    def fn(self):
        print('B fn')
    def __init__(self):
        print('B init')

class A(object):
    def fn(self):
        print('A fn')
    def __new__(cls,a):
        print('new',a)
        if a>10:
            return super(A,cls).__new__(cls)
        return B()
    def __init__(self,a):
        print('init',a)

a1=A(5)

a1.fn()
a2=A(20)
a2.fn()
'''

'''性能 str vs list
import time

def strtest(num):
    start=time.time()
    st='first'
    for i in range(num):
        st+='X'
    ptime=time.time()-start
    print(ptime)
    return st
def  strtest2(num):
    start=time.time()
    st=['first']
    for i in range(num):
        st.append('X')
    aa=''.join(st)
    ptime=time.time()-start
    print(ptime)
    return aa
a=strtest(10000000)
b=strtest2(10000000)
print(b==a)
'''
'''性能 temp
from time import time 
t = time() 
lista = [1,2,3,4,5,6,7,8,9,10] 
listb =[0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,0.01] 
for i in range (1000): 
    for a in range(len(lista)): 
        for b in range(len(listb)): 
            x=lista[a]+listb[b] 
print ("A total run time:")
print (time()-t)

from time import time 
t = time() 
lista = [1,2,3,4,5,6,7,8,9,10] 
listb =[0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,0.01] 
len1=len(lista) 
len2=len(listb) 
for i in range (1000): 
    for a in range(len1): 
        temp=lista[a]
        for b in range(len2): 
            x=temp+listb[b] 
print ("B total run time:")
print (time()-t)
'''

'''List Comprehensions vs generator expressions
from time import time 
t = time() 
list = ['a','b','is','python','jason','hello','hill','with','phone','test', 
'dfdf','apple','pddf','ind','basic','none','baecr','var','bana','dd','wrd'] 
total=[] 
for i in range (1000000): 
    for w in list: 
        total.append(w) 
print ("A total run time:")
print (time()-t)

from time import time 
t = time() 
list = ['a','b','is','python','jason','hello','hill','with','phone','test', 
'dfdf','apple','pddf','ind','basic','none','baecr','var','bana','dd','wrd'] 
total=[] 
for i in range (1000000): 
    a = [w for w in list]
print(type(a))
print ("B total run time:")
print (time()-t)
 
from time import time 
t = time() 
list = ['a','b','is','python','jason','hello','hill','with','phone','test', 
'dfdf','apple','pddf','ind','basic','none','baecr','var','bana','dd','wrd'] 
total=[] 
for i in range (1000000): 
    a = (w for w in list)
print(type(a))
print ("C total run time:")
print (time()-t)
'''
'''
from itertools import zip_longest
a=list((pow, [2, 3, 10], [4, 5, 3]))
print(a)

def fib():
    prev,curr=0,1
'''
'''
def hello_world():
    s=5
    ss='asd'+s
    return 'hello world'
def hello():
    hello_world()
hello()
'''
'''
a=({'a':1,'b':2},{'c':3,'d':4})
#len(a[0])

import hashlib
m = hashlib.md5()
data = "G. T. Wang"
m.update(data.encode("utf-8"))

h = m.hexdigest()
print(h)
'''

a=['a','b','c','d']
a.remove('b')
print(a)