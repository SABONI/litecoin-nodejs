Google Ads:
* setting and concept:
1. type:
    1. ads event
    2. words
    3. picture
    4. video
    5. mobile & app
    6. group(key words group)
2. factor:
    1. geo & lang
    2. search engine
    3. website
3. track


* cost




* google guidelines: https://static.googleusercontent.com/media/guidelines.raterhub.com/zh-TW//searchqualityevaluatorguidelines.pdf
* How Does Google `Evaluate` A Website?http://illuminationconsulting.com/how-does-google-evaluate-a-website/<br>
    1. Website Backlinks:authority websites
    2. Unique and Quality Content
    3. Content Length Matters
    4. Website Loading Speed
    5. Page Optimization
    6. HTML Improvements

* 10 Ways to `Evaluate` Your SEO Progress:https://www.inc.com/jayson-demers/10-ways-to-evaluate-your-seo-progress.html
    1. How many people find the site `through organic search`? You can find this under "Organic Traffic" in Google Analytics; it will tell you how many people found your site through search. This is more important than your keyword rankings (we'll touch on those later) because it tells you how many people `actually clicked through` to your site. 
    2. How many people find the site `through referrals`? 
    3. How many `pages` get at least some search traffic?
    4. How have my `rankings` changed? `keyword rankings` (even with today's Hummingbird/semantic search-driven engines). Noticing a sudden drop in one niche due to an emerging competitor can help you redirect your strategy to a more competitive target. You can track rankings automically using software like AgencyAnalytics, AuthorityLabs, or AdvancedWebRanking.
    5. What `new links` have I earned?  `link profile tool` (such as Open Site Explorer, Majestic, or Ahrefs) to examine what links are pointing to your site;You can also use `BuzzSumo` to get notifications in real-time whenever a new link is found pointing to your site.
    6. How is my content performing?Which `topics` are performing well?
    7. What `social signals` am I getting? Social signals are not as important to your rankings as some people might claim. Getting lots of social shares does little for your rankings directly, but it does serve as an indication of your content's strength, and does carry peripheral benefits (like earning your brand more visibility and links). Pay attention to which pieces of your content get the most social shares, and how those shares grow over time.
    8. How many `conversions` am I getting? An SEO strategy by itself doesn't guarantee conversions (even if it's top-notch); for that you'll need a dose of conversion optimization. However, the number of conversions made by your organic traffic can give you an indication of that traffic's relevance to your brand. If your conversion rates suffer, it could be due to inefficient audience targeting in your SEO strategy.
    9. How have I grown since the beginning? `time`
    10. How much am I spending? `costs`

* tools:https://hsienblog.com/2017/08/12/seo-tools-v1/
* awoo: https://www.awoo.com.tw/blog/category/awooseotool/
