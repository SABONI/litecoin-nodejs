const Neon = require('@cityofzion/neon-js')


const wal = new Neon.wallet.Account()

console.log(wal)
console.log(wal.getPublicKey())
console.log()

Neon.api.neoscan.getBalance(
  'http://api.neoscan.io/api/main_net/',
  'AcJURjmTQbyAhug7BgiYEj9zNGxjz4uiD7',
).then((ba) => {
  console.log(ba)
  const symbols = ba.assetSymbols
  console.log(symbols)
  const neoCoins = ba.assets.GAS.unspent
  // console.log(neoCoins)
  let tmp = 0
  ba.assets.NEO.unspent.forEach((a) => {
    tmp += a.value.toNumber()
  })
  console.log(tmp)
  ba.tokenSymbols.forEach((token) => {
    console.log(token, ':', ba.tokens[token].toNumber())
  })
}).catch((error) => { console.log('caught', error) })


import Neon from "@cityofzion/neon-js";
// Let us create a ContractTransaction
let tx = Neon.create.tx({ type: 128 });
// Now let us add an intention to send 1 NEO to someone
tx.addOutput("NEO", 1, someAddress)
  .addRemark("I am sending 1 NEO to someAddress") // Add an remark
  .calculate(balance) // Now we add in the balance we retrieve from an external API and calculate the required inputs.
  .sign(privateKey); // Sign with the private key of the balance

const hash = tx.hash; // Store the hash so we can use it to query a block explorer.

// Now we can use this serializedTx string and send it through sendrawtransaction RPC call.
const serializedTx = tx.serialize();
