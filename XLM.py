from stellar_base.keypair import Keypair
from stellar_base.utils import StellarMnemonic
from stellar_base.address import Address
from stellar_base.horizon import horizon_livenet, horizon_testnet
import requests
from stellar_base.builder import Builder
'''
# kp = Keypair.random()
sm = StellarMnemonic("chinese")
secret_phrase = sm.generate()
print(secret_phrase)
kp = Keypair.deterministic(secret_phrase, lang='chinese')
print(kp.__dict__)
publickey = kp.address().decode()
print('publickey :' + publickey)
seed = kp.seed().decode()
print('seed :' + seed)

print(kp.from_seed(kp.seed().decode()).address())

# ask friend bot 
url = 'https://friendbot.stellar.org'
res = requests.get(url, params={'addr': publickey})
print(res)

# This creates a new Horizon Livenet instance
horizon = horizon_testnet()
sequence = horizon.account(kp.address().decode())
print(sequence)

address = Address(address=publickey, network='TESTNET') # See signature for additional args
address.get() # Get the latest information from Horizon
print('Balances: {}'.format(address.balances))
print('Sequence Number: {}'.format(address.sequence))
print('Flags: {}'.format(address.flags))
print('Signers: {}'.format(address.signers))
print('Data: {}'.format(address.data))
'''

seed = "SDA5BS5HKL2JH4GDWGE67535QAWVFMS3IFHIQHC2LESIUAWCUUJXJHCM"
builder = Builder(secret=seed)
# builder = Builder(secret=seed, network='public') for LIVENET

bob_address = 'GACXLNX7DHLNRJFJYU26NLCERFT6PJH2T2DZT33T4JAYIFPB2PFN24JT'
builder.append_payment_op(bob_address, '100', 'XLM')
builder.add_text_memo('For beers') # string length <= 28 bytes
builder.sign()

# Uses an internal horizon instance to submit over the network
builder.submit()
