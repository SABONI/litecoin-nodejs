const iotacore = require('@iota/core')

const iota = iotacore.composeAPI({
  // 'provider': 'http://node04.iotatoken.nl:14265',
  'provider': 'https://nodes.devnet.iota.org:443',
})
iota.getNewAddress('HQTKNTQOLESHCSQMQCCCKNSTVIHVAZILOZOKVSXRKQQQRGWULPKCZVKQQCLERBXCXVOOGANWZPKZYKSRP')
  .then((info) => {
    console.log(info)
  }).catch((err) => {
    console.log(err)
  })

iota.getAccountData('HQTKNTQOLESHCSQMQCCCKNSTVIHVAZILOZOKVSXRKQQQRGWULPKCZVKQQCLERBXCXVOOGANWZPKZYKSRP')
  .then((info) => {
    console.log(info)
  }).catch((err) => {
    console.log(err)
  })

iota.getNodeInfo()
  .then((info) => {
    console.log(info)
  }).catch((err) => {
    console.log(err)
  })
// must be truly random & 81-trytes long
const seed = ' your seed here '

// Array of transfers which defines transfer recipients and value transferred in IOTAs.
const transfers = [{
  'address': ' recipient address here ',
  'value': 1000, // 1Ki
  'tag': '', // optional tag of `0-27` trytes
  'message': '', // optional message in trytes
}]

// Depth or how far to go for tip selection entry point
const depth = 3

// Difficulty of Proof-of-Work required to attach transaction to tangle.
// Minimum value on mainnet & spamnet is `14`, `9` on devnet and other testnets.
const minWeightMagnitude = 14

iota.prepareTransfers(seed, transfers)
  .then((trytes) => { iota.sendTrytes(trytes, depth, minWeightMagnitude) })
  .then((bundle) => {
    console.log(`Published transaction with tail hash: ${bundle[0].hash}`)
    console.log(`Bundle: ${bundle}`)
  })
  .catch((err) => {
    // catch any errors
  })
