const litecoin = require('litecoin')

const client = new litecoin.Client({
  'host': 'localhost',
  'port': 19332,
  'user': 'litecoinrpc',
  'pass': 'pass',
})
/*
client.getBalance('*', 6, (err, balance) => {
  if (err) console.error(err)
  console.log('Balance: ' + balance)
})

client.cmd('listaccounts', (err, result, resHeaders) => {
  if (err) return console.log(err)
  console.log('Listaccounts:', result)
})

client.cmd('getaddressesbyaccount', '', (err, result, resHeaders) => {
  if (err) return console.log(err)
  console.log('Address in default acc:', result)
  client.cmd('validateaddress', result[0], (errr, resultt, resHeaderss) => {
    if (err) return console.log(errr)
    // console.log('Address info reslut:', resultt)
  })
  client.cmd('dumpprivkey', result[0], (errrr, resulttt, resHeadersss) => {
    // (optional) account name
    if (err) return console.log(errrr)
    console.log('Address privkey is :', resulttt)
  })
})
*/
const address = ''
const unspents = {}
client.cmd('getaddressesbyaccount', '', (err, result, resHeaders) => {
  if (err) return console.log(err)
  console.log(result)
  for (let a in result) {
    client.cmd('dumpprivkey', result[a], (errr, resultt, resHeaders) => {
      if (errr) return console.log(errr)
      console.log('acc ' + result[a] + ' key is :' + resultt + '.')
    })
  }
})

client.cmd('listunspent', (err, result, resHeaders) => {
  if (err) return console.log(err)
  for (let a in result) {
    console.log('txid: ' + result[a].txid + ', vout: ' + result[a].vout + ', address: ' + result[a].address)
  }
})

client.cmd('dumpprivkey', 'QgZGLrpPdA9q4Hf76Wqz4NzL122gQdHAa4', (err, result, resHeaders) => {
  if (err) return console.log(err)
  console.log('acc QgZGLrpPdA9q4Hf76Wqz4NzL122gQdHAa4 key is :' + result + '.')
})

client.cmd('getaccountaddress', '', (err, result, resHeaders) => {
  if (err) return console.log(err)
  console.log('receicve address is :' + result + '.')
})
/*
client.cmd('getaddressesbyaccount', 'myaccount', (err, result, resHeaders) => {
  if (err) return console.log(err)
  console.log(result)
})
client.cmd('listunspent', (err, result, resHeaders) => {
  // (optional) account name
  if (err) return console.log(err)
  console.log('Address listunspent is :', result)
})
*/
