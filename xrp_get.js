const RippleAPI = require('ripple-lib').RippleAPI

const api = new RippleAPI({
  'server': 'wss://s.altnet.rippletest.net:51233', // Public rippled server hosted by Ripple, Inc.
})
api.on('error', (errorCode, errorMessage) => {
  console.log(errorCode + ': ' + errorMessage)
})
api.on('connected', () => {
  console.log('connected')
})
api.on('disconnected', (code) => {
  console.log('disconnected, code:', code)
})

// generateAddress()
// Account1
// rRrSiUGf67X1KqW3aQHSEaDD2oN8zBqpM
// sniGmBAG4G6UYmye7driYLak8gpDh
// Account2
// rGefFwgvf2HZZLzKkmdwtQgH8fzE26m8Qj
// ss8LFsDfEzorD2czAzZP2yji1QtsQ
api.connect().then(() => {
  /* insert code here */
  return api.getBalances('rRrSiUGf67X1KqW3aQHSEaDD2oN8zBqpM')
}).then((data) => {
  console.log(data)
}).then(() => {
  return api.disconnect()
})
  .then(() => {
    console.log('done and disconnected.')
  })
