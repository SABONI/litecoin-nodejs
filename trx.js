
const TronWeb = require('tronweb')

const HttpProvider = TronWeb.providers.HttpProvider // This provider is optional, you can just use a url for the nodes instead
// const fullNode = 'http://52.53.189.99:8090'
// const fullNode = 'http://52.44.75.99:8090'
const fullNode = 'https://api.trongrid.io:8090'
const event_server = 'https://api.trongrid.io/'
const solidityNode = 'https://api.trongrid.io:8091'
//const solidityNode = 'http://52.44.75.99:8091'
const tronWeb = new TronWeb(
  fullNode,
  solidityNode,
)
tronWeb.isConnected((err, res) => {
  if (err) return console.log(err)
  console.log(res)
})

console.log(tronWeb.createAccount())
// The majority of the function calls are asynchronus
// meaning that they cannot return the result instantly.
// These methods therefore return a promise, which you can await.
/*
tronWeb.createAccount((err, balance) => {
  console.log(balance)
})
*//*
function getBalance() {
  tronWeb.trx.getBalance('TSmb465fW31DmHNuxqJx6uNvgZ46rmjJoi').then((err, balance) => {
    console.log('balance is: ' + balance)
  }).catch((err) => { console.error(err) })
}

getBalance()

/*
// You can also bind a `then` and `catch` method.
tronWeb.getBalance(address).then(balance => {
    console.log({ balance })
}).catch(err => console.error(err))
// If you'd like to use a similar API to Web3, provide a callback function.
tronWeb.getBalance(address, (err, balance) => {
    if(err)
        return console.error(err)
    console.log({ balance })
})
*/
