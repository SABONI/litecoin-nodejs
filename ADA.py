import requests
'''create wallet
url = 'https://127.0.0.1:8090/api/v1/wallets'
data = {
  "operation": "create",
  "backupPhrase": [
    "absurd",
    "amount",
    "divorce",
    "acoustic",
    "abandon",
    "absurd",
    "abandon",
    "ability",
    "acoustic",
    "amount",
    "letter",
    "agree"
  ],
  "assuranceLevel": "strict",
  "name": "My Wallet"
}
head = {
  "Accept": "application/json; charset=utf-8",
  "Content-Type": "application/json; charset=utf-8"
}

walletcreate = requests.post(url, headers=head, json=data, verify=False)
print(walletcreate.content)
'''


getwalletsURL = 'https://127.0.0.1:8090/api/v1/wallets'
getwallets = requests.get(getwalletsURL, verify=False)
print(getwallets.content)

getaddressURL = 'https://127.0.0.1:8090/api/v1/addresses'
getaddress = requests.get(getaddressURL, verify=False)
print(getaddress.content)

'''
tx = {
  "groupingPolicy": <Optional strategy to use for selecting the transaction inputs>,
  "destinations": [{
    "amount": <Amount of coin to bind>,
    "address": <Address to map coins to>
    },
  ],
  "source": {
    "accountIndex": <Corresponding account's index on the wallet>,
    "walletId": <Target wallet identifier to reach>
  },
  "spendingPassword": <Wallet's protection password, required to spend funds if defined>
}

transactionURL = 'https://127.0.0.1:8090/api/v1/transactions'
transaction = requests.post(transactionURL, json=tx, verify=False)
print(transaction.content)
'''