const rpcClientClass = require('monero-rpc-client')
const xmrRpc = require('monero-rpc')
const moneroWallet = require('monero-nodejs')
/*
const Wallet = new moneroWallet()

Wallet.balance().then((result) => {
  console.log(result)
})
*/
const wallet = new xmrRpc.Wallet('http://127.0.0.1:18082')
const daemon = new xmrRpc.Daemon('http://127.0.0.1:38081')

daemon.getLastBlockHeight((err, height) => {
  if (err) return console.log(err)
  console.log(height)
})
daemon.isTestnet((err, testnet) => {
  if (err) return console.log(err)
  console.log(testnet) // false
})
wallet.getAddress((err, address) => {
  if (err) return console.log(err)
  console.log(address)
})
wallet.getBalance((err, balance) => {
  if (err) return console.log(err)
  console.log(balance)
})
/*
const toaddress = '52jRD86CH6djmQDZYtLCNBgYuaUc3gTPBCZuYqbidQ5HCejY5CS7iLJUc2eDpoJrDkieMMuk4bB8cZ5N49cc1RikGmxvRsg'
wallet.transfer({
  'destinations': [{ 'address': toaddress, 'amount': 10000000 }],
  'mixin': 7, // default 7
  'priority': 0, // default 0
}, (err, result) => {
  if (err) return console.log(err)
  console.log(result) // 48958481211
  console.log(result.tx_hash) // '985180f46863...'
})
*/
/*
// monero-rpc-client
// https://getmonero.org/resources/developer-guides/daemon-rpc.html#get_connections
const nodeAddress = 'http://node1.xmr-tw.org:18081'
const rpcClient = new rpcClientClass(nodeAddress)
rpcClient.getBlockCount()
  .then((result) => {
    console.log(result)
  })
  .catch((error) => {
    console.log(error)
  })
*/
