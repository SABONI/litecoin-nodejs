from nemnis import Client, explain_status, Account

nis = Client( endpoint='http://3.16.29.120:7890')
print(repr(nis))
hb = nis.heartbeat()

status = nis.status()

print(status.json())
# you can use following function to get verbose message for status
print(explain_status(status.json()))  
print(hb.status_code)
print(hb.json())
print(nis.account.generate().content)

'''
acc = nis.account.get('NCKMNCU3STBWBR7E3XD2LR7WSIXF5IVJIDBHBZQT')

print(acc.status_code)
print(acc.json())

### You can connect to other nodes just by passing it address:
new_client =  Client('http://157.7.223.222:7890')

new_hb = new_client.heartbeat()

print(new_hb.status_code)
print(new_hb.json())
'''
