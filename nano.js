
const RaiClient = require('node-raiblocks-rpc')

const NODE_ADDRESS = 'http://127.0.0.1:7076'

/**
 * decodeJSON is an optional boolean argument.
 * When set to true (default), the JSON string response is
 * parsed into a JSON object. Otherwise the string is returned.
 */
const client = new RaiClient(NODE_ADDRESS)

client._send('wallet_locked', { 'wallet': '0871C9C0CD2489D74A48A86A5E7DB0762F431BF1C33AE500A2E1EB70B3AF9211' }).then((res) => {
  console.log(res)
}).catch((err) => {
  console.log(err)
})
client._send('wallet_info', { 'wallet': '0871C9C0CD2489D74A48A86A5E7DB0762F431BF1C33AE500A2E1EB70B3AF9211' }).then((res) => {
  console.log(res)
}).catch((err) => {
  console.log(err)
})

/*
client._send('password_enter', { 'wallet': '0871C9C0CD2489D74A48A86A5E7DB0762F431BF1C33AE500A2E1EB70B3AF9211', 'password': 'aa12301230' }).then(
  client.account_create('0871C9C0CD2489D74A48A86A5E7DB0762F431BF1C33AE500A2E1EB70B3AF9211').then((res) => {
    console.log(res)
  }).catch((err) => {
    console.log(err)
  })
).then(
  client._send('wallet_lock', { 'wallet': '0871C9C0CD2489D74A48A86A5E7DB0762F431BF1C33AE500A2E1EB70B3AF9211' })
)
*/
/*
client._send('password_enter', {'wallet': '0871C9C0CD2489D74A48A86A5E7DB0762F431BF1C33AE500A2E1EB70B3AF9211', 'password': 'aa12301230'}).then(
  client.account_list('0871C9C0CD2489D74A48A86A5E7DB0762F431BF1C33AE500A2E1EB70B3AF9211').then((a) => {
    console.log(a)
  }).catch((err) => {
    console.log(err)
  })
).then(
  client._send('wallet_lock', {'wallet': '0871C9C0CD2489D74A48A86A5E7DB0762F431BF1C33AE500A2E1EB70B3AF9211'}).then((res) => {
    console.log(res)
  }).catch((err) => {
    console.log(err)
  })

)
*/
