const RippleAPI = require('ripple-lib').RippleAPI

const api = new RippleAPI({
  'server': 'wss://s.altnet.rippletest.net:51233', // Public rippled server hosted by Ripple, Inc.
})
api.on('error', (errorCode, errorMessage) => {
  console.log(errorCode + ': ' + errorMessage)
})
api.on('connected', () => {
  console.log('connected')
})
api.on('disconnected', (code) => {
  console.log('disconnected, code:', code)
})

// Account1
// rRrSiUGf67X1KqW3aQHSEaDD2oN8zBqpM
// sniGmBAG4G6UYmye7driYLak8gpDh
// Account2
// rGefFwgvf2HZZLzKkmdwtQgH8fzE26m8Qj
// ss8LFsDfEzorD2czAzZP2yji1QtsQ
api.connect().then(() => {
  /* insert code here */
  const payFrom = 'rRrSiUGf67X1KqW3aQHSEaDD2oN8zBqpM'
  const payTo = 'rGefFwgvf2HZZLzKkmdwtQgH8fzE26m8Qj'
  const xrpAmount = 1
  const payment = {
    'source': {
      'address': payFrom,
      'maxAmount': {
        'value': '10',
        'currency': 'XRP',
      },
    },
    'destination': {
      'address': payTo,
      'amount': {
        'value': xrpAmount.toString(),
        'currency': 'XRP',
      },
    },
  }
  return api.preparePayment(payFrom, payment)
}).then((payment) => {
  const secret = 'sniGmBAG4G6UYmye7driYLak8gpDh'
  console.log('Prepare Payment')
  console.log(payment)
  return api.sign(payment.txJSON, secret)
}).then((signedTx) => {
  console.log('Sign')
  console.log(signedTx)
  return api.submit(signedTx.signedTransaction)
})
  .then((txData) => {
    console.log(txData)
    console.log('   >> [Tentative] Result: ', txData.resultCode)
    console.log('   >> [Tentative] Message: ', txData.resultMessage)
  })
  .then(() => {
    return api.disconnect()
  })
  .then(() => {
    console.log('done and disconnected.')
  })
