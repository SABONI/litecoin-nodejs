## Litecoin-nodejs
same way as bitcoin<br>
commond :<br>
https://litecoin.info/index.php/Litecoin_API<br>

## Monero(XMR)
1. monero-wallet-cli.exe<br>
`monero-wallet-cli.exe --stagenet --use-english-language-names`<br>
Create new wallet or open existed wallet.<br>
--stagenet: wallet is bound network(testnet is 28081,stagenet is 38081)<br>
2. monero-wallet-rpc.exe<br>
Open your wallet rpc, and could connect to it with package`monero-rpc`<br>
`monero-wallet-rpc.exe --stagenet --daemon-address xxx.xxx.xxx.xxx:xxxx --wallet-file xxx --disable-rpc-login --rpc-bind-port xxxx --password xxx`<br>
--daemon-address: remote ip<br>
--wallet-file: your wallet<br>
--password: wallet password<br>
--disable-rpc-login: without author<br>
--rpc-bind-port: set your port<br>
3. monerod

## Ripple(XRP)
nodejs code example - https://developers.ripple.com/rippleapi-reference.html<br>

## IOTA
http://www.iotasupport.com/gui-lightwallet.shtml<br>
https://docs.iota.org/<br>
https://faucet.testnet.iota.org/<br>
https://devnet.thetangle.org<br>

## Ontology(ONT)
Use the `ontology-python-sdk` package.<br>
https://apidoc.ont.io/pythonsdk<br>

## Dash(DASH)
Use `@dashevo/dashd-rpc`.<br>
rpc command :https://github.com/dashevo/dashd-rpc/blob/develop/lib/index.js <br>

## Cardano(ADA)
Download the `Daedalus` and sync block.<br>
Use `cardano API` :https://cardanodocs.com/technical/wallet/api/v1/<br>

## NEO
@cityofzion/neon-js<br>

## Tron(TRX)
http://doc.tron.network/<br>
https://github.com/tronprotocol/tron-web/issues/8<br>

## Stellar(XLM)
package`stellar-base`<br>
https://stellar-base.readthedocs.io/en/latest/<br>
http://testnet.stellarchain.io/<br>

## Nano(NANO)
npm `node-raiblocks-rpc`<br>
Download wallet and open<br>
enable rpc C:\Users\<user>\AppData\Local\RaiBlocks\config.json<br>
https://1nano.co/desktop-wallet/<br>
https://github.com/SkyzohKey/node-nano-rpc#methods-names<br>
https://github.com/nanocurrency/raiblocks/wiki/RPC-protocol<br>

## Zcash(ZEC)
package`zcash`<br>
Follow zcash doc: https://zcash.readthedocs.io/en/latest/rtd_pages/troubleshooting_guide.html <br>
Set up node and run `zcashd`<br>
`server=1`,`rpcallowip`,.....should be written in `.zcash/zcash.config`<br>
Rpc command(add method into methods.json): https://zcash-rpc.github.io/z_sendmany.html<br>

