const litecoinCli = require('litecoin')

const client = new litecoinCli.Client({ // 要改成用主鏈
  'host': 'localhost',
  'port': 19332,
  'user': 'litecoinrpc',
  'pass': 'pass',
})
const address = ''
const txId = '419c935362e7d33b421a1ff00cbf9c1d220a3af2e963e8716ea3d06fb4425159'
const toaddress = 'QjYZJJjs2wT7zRb7wfAVjDrZpEqLF1koZE'
const outputAmount = 3
const privatekey = 'cQm4oe8DLpArBF3aBMuZESD544PETU1RRMHdB9zQ437L2E3zFPnZ' // account[0]

client.cmd('gettransaction', txId, (err, txn) => {
  if (err) return console.log(err)
  const inputs = [{ 'txid': txId, 'vout': 1 }]
  const outputs = {}
  outputs[toaddress] = outputAmount
  client.cmd('createrawtransaction', inputs, outputs, (errr, hexstring) => {
    if (errr) return console.log(errr)
    console.log(hexstring)
    const privatekeys = []
    privatekeys.push(privatekey)
    client.cmd('signrawtransaction', hexstring, [], privatekeys, (errrr, signedtxn) => {
      if (errrr) return console.log(errrr)
      console.log(signedtxn)
      client.cmd('sendrawtransaction', signedtxn.hex, (errrrr, result) => {
        if (errrrr) return console.log(errrrr)
        console.log('result:', result)
      })
    })
  })
})
